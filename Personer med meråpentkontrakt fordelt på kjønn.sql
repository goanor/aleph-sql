### Personer med meråpentkontrakt fordelt på kjønn

DECLARE
    v_antall number DEFAULT 0;
    v_antall_m number DEFAULT 0;
    v_antall_k number DEFAULT 0;
    v_antall_u number DEFAULT 0;
    v_antall_x number DEFAULT 0;
BEGIN
    DBMS_OUTPUT.PUT_LINE('Personer med meråpentkontrakt fordelt på kjønn');
    FOR contract IN (SELECT TRIM(SUBSTR(Z304_REC_KEY, 1, 12)) AS rec_key FROM USR01.Z304
		WHERE Z304_TELEPHONE = 'Kontrakt')
        LOOP
            FOR person IN (SELECT COUNT(Z305_BOR_TYPE) AS count,Z305_BOR_TYPE AS gender FROM z305
            	WHERE Z305_REC_KEY LIKE contract.rec_key||'%'
            	GROUP BY Z305_BOR_TYPE)
                LOOP
                	CASE person.gender
                		WHEN 'KV' THEN
                			v_antall_k:=v_antall_k + 1;
                		WHEN 'MA' THEN
                			v_antall_m:=v_antall_m + 1;
                		WHEN 'UT' THEN
                			v_antall_u:=v_antall_u + 1;
                		ELSE
                			v_antall_x:=v_antall_x + 1;
        					DBMS_OUTPUT.PUT_LINE('Uten kjønn: '||contract.rec_key||' '||person.gender||'Type: '||person.gender||' '||person.count);
					END CASE;
           			v_antall:=v_antall + 1;
                END LOOP;
        END LOOP;
        DBMS_OUTPUT.PUT_LINE('Antall menn    : '||v_antall_m);
        DBMS_OUTPUT.PUT_LINE('Antall kvinner : '||v_antall_k);
        DBMS_OUTPUT.PUT_LINE('Antall uten    : '||v_antall_u);
        DBMS_OUTPUT.PUT_LINE('Antall ukjent  : '||v_antall_x);
        DBMS_OUTPUT.PUT_LINE('Totalt antall  : '||v_antall);
END;
/